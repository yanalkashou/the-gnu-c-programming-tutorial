# The GNU C Programming Tutorial

This repository contains tutorial code in **C** from **The GNU C Programming Tutorial [Edition 4.1]** by **Mark Burgess**.

Each folder will contain source code and compiled code for each exercise.
