#include <stdio.h> 	/*  I/O  */
#include <stdlib.h> 	/*  exit  */


/* Function Prototype */
int a_times_b (int a, int b);


int a_times_b (int a, int b)
{
	int c;
	c = a * b;
	return c;
}


int main()
{
	int v1, v2;
	v1 = 9;
	v2 = 81;
	int V;
	V = a_times_b (v1, v2);
	printf ("%d\n", V);

	exit (0);
}


